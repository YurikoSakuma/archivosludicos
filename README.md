# ARCHIVOS LÚDICOS

Un proyecto hecho para recopilar y compartir información sobre videojuegos, este es el link para acceder a la página web: <https://jamepdev.gitlab.io/archivosludicos/>

## ¿Cómo ayudar?
Si gustas aportar, necesitas una cuenta de gitlab, también debes saber un poco de [Markdown](https://www.markdownguide.org/basic-syntax), con esto puedes modificar los archivos que están en el directorio ``/content/docs/``.

## ¿Existe alguna restricción?
Por el momento no hay ninguna bien definida, pero en caso de que vea contenido cuestionable (por así decir) daré la razón de porque no lo permito.

***
Esta página hace uso de la licencia MIT
