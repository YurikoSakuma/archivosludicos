---
title: Ayuda a la página
---
# ¿Cómo ayudar?
<div style="text-align: justify">
<p>Primero tienes que crearte una cuenta en <a href = "https://gitlab.com/users/sign_up">Gitlab</a>, el proceso es bastante sencillo.</p>

<div style="text-align: center">
<p><img src="https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/tutorial1.png"></p>
</div>

Después de que verificaran su cuenta, ya pueden ir al [proyecto](https://gitlab.com/JamepDev/archivosludicos).

<div style="text-align: center">
<img src="https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial2.png">
</div>

Aqui hay dos formas de ayudar

## Proponiendo ideas

En la barra lateral existe una sección llamada "Issues", dan clic ahí y les aparecerá un botón que dice "New issue", lo presionan y aparecerá un formulario donde tendrán que llenar título y descripción de la idea. No aceptaré ideas como "añade x libro/video/youtuber", para eso está la opción de añadir contenido.

## Añadir contenido
Si solo quieres ayudar añadiendo algún libro, video, canal de Youtube o hasta añadir una nueva sección. La carpeta que solo debes tomar en cuenta es la de [content/docs](https://gitlab.com/JamepDev/archivosludicos/-/tree/master/content/docs) ahí se encuentran los archivos que tienen las listas.

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial3.png">
</div>

Seleccionan el archivo que quieran modificar, y presionan el botón “Open in Web IDE”, la página les preguntara que si quieren hacer un “fork” del proyecto, ustedes dicen que sí.

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial5.png">
</div>

Esto les va a abrir un editor donde van a poder modificar el archivo.

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial6.png">
</div>

Ahora, ¿Qué formato usan estos archivos?<br>
Este usa el lenguaje Markdown, que es bastante sencillo y ordenado, con saber la [sintaxis básica](https://www.markdownguide.org/basic-syntax/) basta para esta página. Así que no creo que se tenga dificultad en este aspecto.

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial4.png">
</div>


Después de que hayan hecho el cambio que desean, en el editor se encuentra el botón "Create commit..."

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial7.png">
</div>

Les aparecerá esta ventana, forzosamente tiene que estar en la opción "Create a new branch" y debe estar activada la opción de "Start a new merge request", de caso contrario los cambios que hicieron solo se quedaran para su propia versión del proyecto, presionan el botón de "Commit".

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial8.png">
</div>

Ahora les cargará una nueva página donde harán una solicitud para hacer los cambios, pueden añadir un título y descripción si gustan (de todas maneras revisaré el código para ver hicieron realmente).

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial9.png">
</div>

Con esto solo presionan el botón “Create merge request”, con esto la responsabilidad queda en mi de aceptar su cambio o rechazarlo.

<div style="text-align: center">
<img src= "https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Tutorial10.png">
</div>


Como aviso, les aparecerá estos mensajes cada que hagan un cambio, no significa que hicieron algo mal, simplemente ignórenlas.

<div style="text-align: center">
<img src="https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Advertencia.png">
<img src="https://gitlab.com/JamepDev/archivosludicos/-/raw/master/static/Advertencia2.png">
</div>

Con esto termino este pequeño tutorial, espero que les sea de ayuda y puedan aportar a este pequeño proyecto.</div>
