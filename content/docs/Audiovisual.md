---
title: Audiovisual
---
# Audiovisual

## Youtube
### Canales
#### Inglés
- [ActionButton](https://www.youtube.com/c/ActionButton)
- [Ahoy](https://www.youtube.com/user/XboxAhoy)
- [Core-A Gaming](https://www.youtube.com/c/CoreAGaming)
- [Cutscenes](https://www.youtube.com/channel/UC1LbVYK0KwVrX9ke3i0tpfQ)
- [Errant Signal](https://www.youtube.com/user/Campster)
- [The Game Overanalyser](https://www.youtube.com/channel/UCZMF14eNxvuReRTceX_mbqQ)
- [TheGamingBritShow](https://www.youtube.com/user/TheGamingBritShow)
- [Gaming Historian](https://www.youtube.com/user/mcfrosticles)
- [Ghenry Perez](https://www.youtube.com/user/GhenryPerez)
- [InfernoPlus](https://www.youtube.com/user/dmtinfernocide)
- [Jeremy Parish | Video Works](https://www.youtube.com/c/JeremyParish)
- [Leon Massey](https://www.youtube.com/user/MeowsyOnEUW)
- [Maraganger](https://www.youtube.com/@maraganger)
- [Masahiro Sakurai on Creating Games](https://www.youtube.com/channel/UCv1DvRY5PyHHt3KN9ghunuw)
- [Matthewmatosis](https://www.youtube.com/user/Matthewmatosis)
- [Modern Vintage Gamer](https://www.youtube.com/c/ModernVintageGamer)
- [NakeyJakey](https://www.youtube.com/@NakeyJakey)
- [Nerrel](https://www.youtube.com/channel/UCZKyj7wDE51SMbkrRBT6SdA)
- [NeverKnowsBest](https://www.youtube.com/channel/UC1fKT0wuhchtclPqpdWEnHw)
- [Noah Caldwell-Gervais](https://www.youtube.com/user/broadcaststsatic)
- [Noclip - Video Game Documentaries](https://www.youtube.com/channel/UC0fDG3byEcMtbOqPMymDNbw)
- [People Make Games](https://www.youtube.com/c/PeopleMakeGames)
- [RagnarRox](https://www.youtube.com/user/RagnarRoxShow)
- [ROM: Read Only Museum](https://www.youtube.com/channel/UC5FcNtu_P4t7IkU_Izh7cVQ)
- [SoberDwarf](https://www.youtube.com/channel/UCs595r4A30fYDOd7AzTAgbw)
- [SomaSpice](https://www.youtube.com/channel/UCUEpVC_Ia7fYhy426DvezSQ)
- [Super Bunnyhop](https://www.youtube.com/c/bunnyhopshow)
- [SsethTzeentach](https://www.youtube.com/c/SsethTzeentach)
- [Stop Skeletons From Fighting](https://www.youtube.com/c/StopSkeletonsFromFighting)
- [ThorHighHeels](https://www.youtube.com/user/ThorHighHeels)

#### Español
- [Alejandro Julián](https://www.youtube.com/user/AlejandroJSL)
- [BeetBeatBit](https://www.youtube.com/channel/UCvVOxuJqDgGkL683uvkOCGg)
- [Chiguiro Renegado](https://www.youtube.com/c/ChiguiroRenegado)
- [Coalición Pixel](https://www.youtube.com/channel/UChn7IS0X7C7_IvCDym_P90A)
- [Guinxu](https://www.youtube.com/c/Guinxu)
- [Hugo M. Gris](https://www.youtube.com/channel/UCaLz_puKCmed5kEkuJzSgpA)
- [James Blood](https://www.youtube.com/c/JamesBlood45)
- [Jamep](https://www.youtube.com/channel/UCQZpHLb_lQK6ShUFIoZ0zgw)
- [Joseju](https://www.youtube.com/channel/UCzDI_VuYb14FLu9f8uXOeSA)
- [Kelzor](https://www.youtube.com/channel/UCteAOKffwKZlPlZTXlzwKmQ)
- [LeonGuasJir](https://www.youtube.com/user/LeonGuasJir)
- [Loco Damián](https://www.youtube.com/user/supermordecai68)
- [Maldito Mur](https://www.youtube.com/channel/UCOpVuDFNo_ttOh7KwE7bOuw)
- [Matiense](https://www.youtube.com/channel/UCGyyyeo7J8wAbj_P9a1xbLg)
- [La Mesita Anacrónica](https://www.youtube.com/c/LaMesitaAnacr%C3%B3nica)
- [MightyRengar](https://www.youtube.com/c/MightyRengarrr)
- [Pabloide](https://www.youtube.com/channel/UCgQ7_cgUWWTi4oQCAlezj1A)
- [pixel2pixel](https://www.youtube.com/user/pixel2pixel)

### Videos
- [Classic Game Postmortem: Robotron: 2084](https://youtu.be/90GuCjmNzVI)
- [Classic Game Postmortem: Yars' Revenge](https://www.youtube.com/watch?v=aqH4k_OEqhY)
- [Chris Crawford Dragon Speech](https://www.youtube.com/watch?v=CBrj4S24074)
- [Put Your Name on Your Game, a Talk - Bennett Foddy and Zach Gage](https://www.youtube.com/watch?v=N4UFC0y1tY0)
- [State of the Union - Bennett Foddy](https://www.youtube.com/watch?v=7XfCT3jhEC0)

